from PIL.Image import Image

class PlacedElement:
    def __init__(self, element: Image, slot: dict):
        self.element = element
        self.slot = slot
