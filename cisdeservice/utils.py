import random
from PIL import Image
from .PlacedElement import PlacedElement
import cisdeservice.constants as constants

def place_elements(element_list: list) -> list:
    slots = (
        {'placement': (0,0), 'rotation': constants.BASE_ANGLE}, # Top Left
        {'placement': (constants.IMAGE_WIDTH,0), 'rotation': constants.BASE_ANGLE * -1}, # Top Right
        {'placement': (constants.IMAGE_WIDTH,constants.IMAGE_WIDTH), 'rotation': constants.BASE_ANGLE}, # Bottom Right
        {'placement': (0, constants.IMAGE_WIDTH), 'rotation': constants.BASE_ANGLE * -1}, # Bottom Left

        {'placement': (constants.IMAGE_WIDTH / 2, 0), 'rotation': 0}, # Top Middle
        {'placement': (constants.IMAGE_WIDTH, constants.IMAGE_WIDTH / 2), 'rotation': 0}, # Middle Right
        {'placement': (constants.IMAGE_WIDTH / 2, constants.IMAGE_WIDTH), 'rotation': 0}, # Bottom Middle
        {'placement': (0, constants.IMAGE_WIDTH / 2), 'rotation': 0} #Middle Left
    )
    total_slots = len(slots)
    half_slots = int(total_slots / 2)

    available_slot = [*range(half_slots)] if len(element_list) <= half_slots else [*range(total_slots)]
    random.shuffle(available_slot)

    placed_elements = []
    for element in element_list:
        selected_slot = slots[available_slot.pop()]
        placed_elements.append(PlacedElement(element, selected_slot))

    return placed_elements

def resize_element(element: Image.Image, size: int) -> Image.Image:
    bigger_side = element.width if element.width >= element.height else element.height
    ratio = bigger_side / size

    width = int(round(element.width / ratio))
    height = int(round(element.height / ratio))

    return element.copy().resize((width, height), resample=Image.LANCZOS)

def rotate_element(placed_element: PlacedElement) -> PlacedElement:
    large_wobble = random.randint(10, 35)
    small_wobble = random.randint(-10, 10)

    base_rotation = placed_element.slot['rotation']
    course = -1 if base_rotation == constants.BASE_ANGLE else 1

    final_rotation = 0
    is_corner = base_rotation in (constants.BASE_ANGLE, constants.BASE_ANGLE * -1)
    if is_corner:
        final_rotation = base_rotation + (large_wobble * course)

    if not is_corner:
        final_rotation = base_rotation + small_wobble

    rotated = placed_element.element.copy().rotate(final_rotation, expand=True,
                                                   resample=Image.BICUBIC)

    return PlacedElement(rotated, placed_element.slot)

def calc_coordinates(position: int, length: int) -> int:
    wobble = random.randint(0, 10)

    if position == 0:
        final = wobble

    if position == constants.IMAGE_WIDTH:
        final = position - length - wobble

    if position == constants.IMAGE_WIDTH / 2:
        course = random.randrange(-1, 1, 2)
        final = (constants.IMAGE_WIDTH / 2) - (length / 2) + ((wobble / 2) * course)

    return int(round(final))

def palette() -> tuple:
    palette_list = [
        ('#CFF09E', '#79BD9A'),
        ('#A2DEE8', '#92A5D6'),
        ('#FCF4D3', '#FCF4D3'),
        ('#FDA88B', '#FFFDD3'),
        ('#E3AAD6', '#FFBDD8'),
        ('#FF3D7F', '#FF9E9D'),
        ('#E8F3F8', '#DBE6EC'),
        ('#F54828', '#F8E4C1'),
        ('#D6FF9F', '#8DBD0C'),
        ('#FF3D7F', '#FF9E9D'),
        ('#9d93cd', '#6e5fb5'),
        ('#870669', '#fba5e7'),
        ('#6f97ff', '#0d50ff'),
        ('#2ab272', '#17633f'),
        ('#f3ad7a', '#653009'),
        ('#fbbaa4', '#992c07'),
        ('#662640', '#e0abc1'),
        ('#F1D1E9', '#962e7c'),
        ('#fffcd2', '#d2c400'),
        ('#d2f58c', '#53780a'),
        ('#ffb0b0', '#b00000')
    ]

    last_palette = len(palette_list) - 1
    palette_number = random.randint(0, last_palette)
    return palette_list.pop(palette_number)
