#!/bin/env python3

import random
from argparse import ArgumentParser, FileType
from PIL import Image

import cisdeservice.utils as utils
from cisdeservice import constants as cisdeservice

def main():
    parser = ArgumentParser()
    parser.add_argument('type', nargs='+')
    parser.add_argument('-o', '--output', default='/tmp/')

    args = parser.parse_args()

    background_color, foreground_color = utils.palette()
    base_image = Image.new(mode='RGBA',
                           size=(cisdeservice.IMAGE_WIDTH, cisdeservice.IMAGE_WIDTH),
                           color=background_color)

    elements_list = []
    for element_path in args.type:
        element = Image.open(element_path).convert('RGBA')
        elements_list.append(utils.resize_element(element, cisdeservice.ELEMENT_SIZE))

    placed_elements = utils.place_elements(elements_list)

    for placed_element in placed_elements:
        rotated_element = utils.rotate_element(placed_element).element
        x_axis = utils.calc_coordinates(placed_element.slot['placement'][0], rotated_element.width)
        y_axis = utils.calc_coordinates(placed_element.slot['placement'][1], rotated_element.height)

        base_image.paste(rotated_element, (x_axis, y_axis), rotated_element)

    foreground = Image.new(mode='RGBA',
                           size=(cisdeservice.FOREGROUND_WIDTH, cisdeservice.FOREGROUND_WIDTH),
                           color=foreground_color)
    foreground_alpha = Image.new(mode='L', size=(cisdeservice.FOREGROUND_WIDTH, cisdeservice.FOREGROUND_WIDTH), color=128)
    foreground_position = int((cisdeservice.IMAGE_WIDTH - cisdeservice.FOREGROUND_WIDTH) / 2)
    base_image.paste(foreground,(foreground_position, foreground_position), foreground_alpha)

    output = "{}{}".format(args.output, "output-1.png")
    base_image.save(output)

if __name__ == '__main__':
    main()
